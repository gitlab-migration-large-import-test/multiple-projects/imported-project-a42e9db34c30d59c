# frozen_string_literal: true

class ProjectRegistrationJob < ApplicationJob
  queue_as :low

  sidekiq_options retry: false

  def perform
    run_within_context({ job: "project-registration" }) do
      log(:info, "Triggering project registration/update")
      Dependabot::Projects::Registration::Service.call
    end
  end
end
