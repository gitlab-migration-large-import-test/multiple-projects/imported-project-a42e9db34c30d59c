# frozen_string_literal: true

module Dependabot
  module MergeRequest
    module ServiceMode
      module CreateService
        # In deploy mode mr is accepted async based on status of pipeline run
        # Revert to setting auto-merge if app is not integrated
        #
        # @return [void]
        def accept_mr
          super unless AppConfig.integrated?
        end
      end
    end
  end
end
