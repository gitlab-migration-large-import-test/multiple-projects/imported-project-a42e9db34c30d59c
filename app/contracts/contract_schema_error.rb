# frozen_string_literal: true

class ContractSchemaError < StandardError
  def self.format(result, separator = "\n")
    result.errors.group_by(&:path).map do |path, messages|
      "key '#{path.join('.')}' #{messages.map(&:dump).join('; ')}"
    end.join(separator)
  end
end
