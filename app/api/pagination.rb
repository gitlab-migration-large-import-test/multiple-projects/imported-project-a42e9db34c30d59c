# frozen_string_literal: true

module Pagination
  def self.included(base)
    base.class_eval do
      include Grape::Kaminari

      helpers do
        params :pagination_params do
          use :pagination, max_per_page: 100
        end
      end
    end
  end
end
