# frozen_string_literal: true

describe V1::NotifyRelease, type: :request do
  subject(:trigger_update) do
    api_post("/api/notify_release", { name: dependency_name, package_ecosystem: package_ecosystem })
  end

  include_context "with api helper"

  let(:dependency_name) { "rspec" }
  let(:package_ecosystem) { "bundler" }

  before do
    allow(NotifyReleaseJob).to receive(:perform_later)
  end

  it "triggers updates" do
    trigger_update

    expect_status(201)
    expect_json(triggered: true)
    expect(NotifyReleaseJob).to have_received(:perform_later).with(dependency_name, package_ecosystem)
  end
end
